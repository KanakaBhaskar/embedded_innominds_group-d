/*===========================================================================================
	This program is for: A person is wants to go from a source floor to destionation floor 
	by using Elevator and the actions are performed either his goes upstaires or downstaires.

	GROUP-D

	Bhaskar Pedapudi
	Emp id:10362

	Aravind Maddala
	Emp id:10364
===========================================================================================*/

#include<iostream>
#include<cstdlib>
#include<unistd.h>


using namespace std;



class Elevator {

	int lift_position;	//current position of lift
	int count;		//count the numbet of persons to entered into the lift
	int current_floor;	//outside button
	int dest_floor;
public:
	
	Elevator(int current_floor,int dest_floor) {
	this->current_floor = current_floor;
	this->dest_floor = dest_floor;	
    }	


        // member function 	
	void enterCurrentFloor();
	void move_up();
	void move_down();
};

   void Elevator:: enterCurrentFloor() {

	
	if(current_floor < 0 && dest_floor > 5) {
		cout<<"Enter valid floor"<<endl;
		exit(0);
	}	
	/*cnto the lift. 2.Quit
		greater or lesser than the destination floor
	*/
	if(current_floor-dest_floor < 0) {
		move_up();
	}
	else if(current_floor - dest_floor > 0) {
		move_down();
	}
	else {
		cout<<"you are on the same floor"<<endl;
	}
        
   }
	

// funtions for moving the lift upwards
   void Elevator::move_up() {

	for ( int i= current_floor+1;i<=dest_floor;i++) {
		sleep(3);
		cout<< i <<"floor"<<endl;
	}
	cout<<"Please close the door <=^=>"<<endl<<endl;
   }
//function for moving downwards

   void Elevator:: move_down() {
	for( int i = current_floor-1; i>=dest_floor; i--) {
		sleep(3);
		cout<<i<<"floor"<<endl;
	}
	cout<<"Please close the door <=^=>"<<endl<<endl;
   }
	

int main(void) {

/*elevator obj
	here we are taken as every instance is a person
*/ 



do {
	cout<<"1. Enter into the lift. 2.Quit"<<endl;
	int choice;
	cin >> choice;
	int current_floor,dest_floor;
	switch(choice) {
		
		case 1:	
			{
			cout<<"Enter Key for the lift :";
			cin>>current_floor;
			cout<<endl;
			cout<<"Enter destination floor :";
			cin>>dest_floor;
			Elevator obj(current_floor,dest_floor);
			obj.enterCurrentFloor();
			}
			break;
		case 2: exit(0);
			break;
	
		default: cout<<"Invalid choice";
				
 	}
}while(1);



//present floor of lift
//nearer lift to enter person	
//check the persons && his weight
//which floor they want to go
//going up/down

}
/*==============================================
OUTPUT:
1. Enter into the lift. 2.Quit
1
Enter Key for the lift :4

Enter destination floor :8
5floor
6floor
7floor
8floor
Please close the door <=^=>

1. Enter into the lift. 2.Quit
1
Enter Key for the lift :4

Enter destination floor :1
3floor
2floor
1floor
Please close the door <=^=>

1. Enter into the lift. 2.Quit
^C



================================================*/
