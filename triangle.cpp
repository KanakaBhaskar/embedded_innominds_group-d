/* 
	Group-D
	Bhaskar Pedapudi
	id : 10362
	Aravind Maddala
	id : 10364
*/

/* Question :
	In this program we have found the no of equilateral triangles that are randomly generated in a matrix;
  	The vertexes should be non zero values, then only the triangles are considered.
	If vertex is zero then that triangle is not considered.
*/

#include<cstdio>
#include<cstdlib>
#include<unistd.h>
#include<iostream>
#include<vector>
using namespace std;
//creating the Mytriangle class
class Mytriangle {

	private:
		int row,column,i,j,cnt;
		//creating vector for two-dimensional array
		vector<vector<int> > matrix; 
	public: 
		//creating the Mytriangle constructor
		Mytriangle() {
			//random function for non repeated random numbers
			srand(getpid()); 
			//random function to generate the random numbers
			row=random()%10; 
			column=random()%10;

			
			for(int i = 0; i < row; ++i) {
			matrix.push_back(vector<int>());
				for(j=0;j<column;j++)
					matrix[i].push_back(random()%10);
			}
		}
		//printing the matrix
		void printRandomlyGeneratedMatrix() {
			for(i=0;i<row;i++,cout<<endl) {
				for(j=0;j<column;j++) {
					cout<<matrix[i][j]<<" ";
				}
			}
		}
		//this method is used for finding all the triangles in a generated matrix
		void findTrianglesInMatrix() {
			int n;
			cnt=0;
			int count=0,flag=0;
			//rotating the for loop for row -1 times for no of hubs
			for(n=1;n<=row-1;n++) { 
				count=0;
				flag=0;
				for(i=0;i<row-n;i++) {
					for(j=n;j<column-n;j++) {

						if(matrix[i][j]>0) {

							if(matrix[i+n][j-n]>0 && matrix[i+n][j+n]>0) {
								cnt++;
								count++;
								flag=1;
							}

						}

					}

				}
			if(flag)
			//printing individual triangles in a corresponding hub
			cout<<"no.of traingles in "<< n <<" hub is "<< count <<endl; 
			
			}
			//printing the total no  of triangles
			cout<<"no.of triangles: "<< cnt<<endl; 
		}

};

int main(void) {
	//creating object for Mytriangle
	Mytriangle mytriangle;
	mytriangle.printRandomlyGeneratedMatrix();
	mytriangle.findTrianglesInMatrix();
	return 0;
}
/*===================================================
OUTPUT:

1 5 0 6 0 3 7 2 
7 7 4 0 0 9 3 7 
8 2 7 9 7 7 4 8 
6 1 7 3 2 5 3 5 
2 3 1 2 9 9 6 8 
.of traingles in 1 hub is 18
no.of traingles in 2 hub is 8
no.of traingles in 3 hub is 1
no.of triangles: 27
====================================================*/
