/****************************************************************************
	This program is to perform a bit wise operations to the given data.
	GROUP-D
        ---------------
	Aravind Maddala 
	Emp id:10364
        ---------------
	Bhaskar Pedapudi
	Emp id:10362
****************************************************************************/

#include<iostream>

#include<cstdlib>


using namespace std;

//class for bitsetmanuplation
class BitsetManuplation {

private:
	//instance variable of bitsetmanuplation class
	int data;
public:
	//parameterized constructor for BitsetManuplation
	BitsetManuplation(int data) {	
		this->data = data;
	}
	void decimal_to_binary();
	void displayBinaryBits();
	void setBitNumberForData(int ); //
	void clearBitForData(int );
	void complementBitForData(int );
};

//Method for displaying the content
void BitsetManuplation::displayBinaryBits() {

        cout<<"{ ";
	for(int i=31; i>=0; i--) {
	
		cout<<"'"<< ((data >> i) & 1)<<"'"<<" " ;
	}
	cout<<"}";
	cout<<endl;
}

//Method for setting the corresponding bit in the data
void BitsetManuplation::setBitNumberForData(int bit_position) {

	cout<<"Set the bit position at:"<<"'"<<bit_position<<"'"<<endl;
	
	data = data | (1 << bit_position); 

	displayBinaryBits();
}

//Method for clear the corresponding bit in the data
void BitsetManuplation::clearBitForData(int bit_position) {

	cout<<"Cleared bit position at:"<<"'"<<bit_position<<"'"<<endl;
	data = data & (~(1 << bit_position));

	displayBinaryBits();

}

//method  for complementing the data
void BitsetManuplation::complementBitForData(int bit_position) {

	cout<<"Complemented bit position at:"<<"'"<<bit_position<<"'"<<endl;
	data = data ^ (1 << bit_position);

	displayBinaryBits();

}

int main(void) {

	int data;
	cout<<"Enter the data:";
	cin>>data;	

	BitsetManuplation bit(data);	//creating an parameterized object
	bit.displayBinaryBits();
	do {
	cout<<"Enter KEY to be perform the operation :"<<endl;
	cout<<"1.SET THE BIT\n2.CLEAR THE BIT\n3.COMPLEMENT THE BIT\n4.EXIT"<<endl;

	int choice;
	cin>>choice;

	switch(choice) {
	
	  case 1:{ 
		int bit_position;
		cout<<"Enter the positon to set:";
		cin>>bit_position;
		bit.setBitNumberForData(bit_position);
		}
		break;
	  case 2:{
	 	int bit_position;
		cout<<"Enter the position to clear:";
		cin>>bit_position;
		bit.clearBitForData(bit_position);
		}
		break;
	  case 3: {
		int bit_position;
		cout<<"Enter the position to complement:";
		cin>>bit_position;
		bit.complementBitForData(bit_position);
		}
		break;
	  case 4: 
		exit(0);
		
	  default: cout<<"invalid choice you entered:";
	}
	}while(1);	
  return 0;
}
/*=======================================
OUTPUT:

Enter the data:244
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
Enter the data:244
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
1
Enter the positon to set:5
Set the bit position at:'5'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
2
Enter the position to clear:4
Cleared bit position at:'4'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '0' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
1
Enter the positon to set:5
Set the bit position at:'5'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
2
Enter the position to clear:4
Cleared bit position at:'4'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '0' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
Enter the data:244
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
1
Enter the positon to set:5
Set the bit position at:'5'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '1' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
2
Enter the position to clear:4
Cleared bit position at:'4'
{ '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '0' '1' '1' '1' '0' '0' '1' '0' '0' }
Enter KEY to be perform the operation :
1.SET THE BIT
2.CLEAR THE BIT
3.COMPLEMENT THE BIT
4.EXIT
==========================================*/
